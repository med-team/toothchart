<?php
/************************************************************************/
/* Tooth Chart                    (index.php)                           */
/************************************************************************/

/************************************************************************/
/* Prints teeth eruption per date using the follow Letter Symbols       */
/* 									*/
/*				Top Jaw					*/
/*									*/
/*				A B C D					*/
/*				E     F					*/
/*			Left	G     H	  Right				*/
/*				I     J					*/
/*									*/
/*				K     L					*/
/*			Left	M     N	  Right				*/
/*				O     P					*/
/*				Q R S T					*/
/*									*/
/*			       Bottom Jaw				*/
/*									*/
/************************************************************************/

/************************************************************************/
/*  To use, simply put the dates of eruption in the formar YYYY-MM-DD   */
/*  in the section provided (Line 60 or thereabouts)			*/
/*									*/
/*									*/
/*									*/
/************************************************************************/


    /** An array describing teeth - Need to put this in a language file sometime **/
    /** Don't really know when we'll use this, but needed to start off somewhere **/
    $teeth_description = array(
			    A=>"Top Left Lateral Incisor (Eye Tooth)", 
			    B=>"Top Left Central Incisor", 
			    C=>"Top Right Central Incisor", 
			    D=>"Top Right Lateral Incisor (Eye Tooth)", 
			    E=>"Top Left Cuspid (Canine)", 
			    F=>"Top Right Cuspid (Canine)", 
			    G=>"Top Left First Molar", 
			    H=>"Top Right First Molar", 
			    I=>"Top Left Second Molar", 
			    J=>"Top Right Second Molar", 
			    K=>"Bottom Left Second Molar",
			    L=>"Bottom Right Second Molar", 
			    M=>"Bottom Left First Molar", 
			    N=>"Bottom Right First Molar", 
			    O=>"Bottom Left Cuspid (Canine)", 
			    P=>"Bottom Right Cuspid (Canine)",
			    Q=>"Bottom Left Lateral Incisor",
			    R=>"Bottom Left Central Incisor",
			    S=>"Bottom Right Central Incisor", 
			    T=>"Bottom Right Lateral Incisor", 
			    );

/** Enter your dates here **/

    /** An Array of dates when the teeth erupted **/
    /** need to put this in a database or something so users can edit it **/
    $erupted = array(
			    A=>"2002-05-09", 
			    B=>"", 
			    C=>"2002-06-25", 
			    D=>"2002-03-23", 
			    E=>"", 
			    F=>"", 
			    G=>"", 
			    H=>"", 
			    I=>"", 
			    J=>"", 
			    K=>"", 
			    L=>"", 
			    M=>"",
			    N=>"", 
			    O=>"", 
			    P=>"",
			    Q=>"",
			    R=>"2002-01-28", 
			    S=>"2002-01-21", 
			    T=>"",
			    			    
/* debug with all teeth on 
			    A=>"0000-00-00",
			    B=>"0000-00-00",
			    C=>"0000-00-00",
			    D=>"0000-00-00",
			    E=>"0000-00-00",
			    F=>"0000-00-00",
			    G=>"0000-00-00",
			    H=>"0000-00-00",
			    I=>"0000-00-00",
			    J=>"0000-00-00",
			    K=>"0000-00-00",
			    L=>"0000-00-00",
			    M=>"0000-00-00",
			    N=>"0000-00-00",
			    O=>"0000-00-00",
			    P=>"0000-00-00",
			    Q=>"0000-00-00",
			    R=>"0000-00-00",
			    S=>"0000-00-00",
			    T=>"0000-00-00", 
			    
			    end debug with all teeth on */
			    );
    /** If the date is empty, leave this space open - Need to set this per font I guess?**/ 
    $Datespace = str_repeat ("&nbsp",19);

/** Start the main program here
    /** Set the background color to beige for the effect of teeth that are highlighted (i.e. white on white won't work!) **/
    echo "<body bgcolor=\"#FEF0C5\">";
    echo "<P align=CENTER>";


    /** First display the first two dates (middle teeth) **/ 
    if (empty($erupted[B])){echo $Datespace," ";} else {echo $erupted[B]," ";};
    if (empty($erupted[C])){echo $Datespace,"<BR>";} else {echo $erupted[C],"<BR>";};

    /** Now print the images as follows: A1.gif if erupted (i.e. has date) or A0.gif if no date  **/

    foreach ($erupted as $tooth => $date) {

	/** set the first part of IMAGE to be the tooth letter (A through T) **/
	$image = $tooth;

	
	/** If the tooth doesn't have a date attached to it, then make the image A0, else make it A1 (A being the tooth letter)**/
	if (empty($date)){$image .= '0';} else {$image .= '1';};

	/** Display the Date $date if it exists for teeth EGIKMO **/
	if (ereg("[A|E|G|I|K|M|O|Q]",$image)) {

		if (empty($date)){echo $Datespace;} 
		    else {echo $date;};
	
	};
	
	/** Display the tooth picture **/
	echo "<IMG ALIGN=CENTER SRC=\"images/".$image.".gif\">";

	/** If the tooth is either DFHJLNPT then echo date and start on a new line **/
  	if (ereg("[D|F|H|J|L|N|P|T]",$image)) {
	
		if (empty($date)){echo $Datespace,"<BR>";} 
		    else {echo $date,"<BR>";};
	
		
	};

	/** If the tooth is J then print an extra space **/
  	if (ereg("J",$image)) {echo "<BR>";};

    }

    /** Now display the last two dates (middle teeth) with the spacer graphic) **/ 
    if (empty($erupted[R])){echo "0000-00-00 ";} else {echo $erupted[R]," ";};
    if (empty($erupted[S])){echo "0000-00-00<BR>";} else {echo $erupted[S],"<BR>";};
    echo "</p>;"  
?>